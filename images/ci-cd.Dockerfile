ARG NODE_VERSION
FROM node:${NODE_VERSION} as build-image

WORKDIR /app

COPY package*.json ./

RUN yarn

COPY . .

RUN yarn build

FROM node:${NODE_VERSION} as final-image

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /app

COPY package*.json ./

RUN yarn --prod

COPY --from=build-image /app/dist ./dist

CMD ["yarn", "start:prod"]