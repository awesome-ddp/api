# Awesome DDP API
API (name TBD) is a server-side app for Awesome DDP.  
DDP (name TBD) is a project of a digital distribution platform where the video-connection is optimized for a different Internet connection quality.
## Installation

```bash
$ yarn
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
